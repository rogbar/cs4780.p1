/*
 * Author: Roger Barker
 * Due: 7/15/2014
 * CS4780 p1
 * 3 way encryption algorithm
 */

#include "threeWay.h"

int main(int argc, char * argv[]){
	string key = "";
	string ascii;
	string result;
	word buffer[12];
	word wordKey[3];
	long number;

	if( argc != 2){
		printf("%s key, where key is 12 chars",argv[0]);
	}

	//assign key to str key
	key = argv[1];
	cout << "Enter Message:\n";
	getline(cin,ascii);

	number = (ascii.length() / 12);
	for(int i = 0; i < number; i++){
		//sread(buffer,12,1,ascii);
		buffer[0] = ascii[0];
		ascii = ascii.substr(1,ascii.length());
		encrypt(buffer,wordKey);
		result += buffer[0];
	}
	//ascii = convertHexToAscii(key);
	cout << hex << result << endl;
	return 0;
}

string convertHexToAscii(string hexVal){
	string str = "";

	if(hexVal.length()%2 == 1){
		hexVal = "0" + hexVal;
	}

	for(int i = 0; i < hexVal.length() -1; i+=2){
		string output = hexVal.substr(i,2);
		char chr = (char)(int)strtol(output.c_str(),NULL,16);
		str.push_back(chr);
	}
	return str;
}

/*
 * pi1 does A[0] = A[0] rotr 10 && A[2] rotl 1
 */
void pi1( word * a){
	a[0] = ((a[0] >> 10) ^ (a[0] << 22));  //A[0] rotr 10
	a[2] = ((a[2] << 1 ) ^ (a[2] >> 31));  //A[2] rotl 1
}

/* 
 * pi2 does A[0] = A[0] rotl 1 && A[2] rotr 10
 */
void pi2( word * a){
	a[0] = ((a[0] << 10) ^ (a[0] >> 22));  //A[0] rotl 1
	a[2] = ((a[2] >> 1 ) ^ (a[2] << 31));  //A[2] rotr 10
}

/*
 * gamma does A[0] = A[0] xor (A[1] or not A[2])
 * and does the same for A[1] and A[2] with a
 * circular shift on the indeces
 */
void gamma( word * a){
	a[0] = (a[0] ^ (a[1] | ~a[2])); //A[0] = A[0] xor (A[1] or not A[2])
	a[1] = (a[1] ^ (a[2] | ~a[0])); //A[1] = A[1] xor (A[2] or not A[0])
	a[2] = (a[2] ^ (a[0] | ~a[1])); //A[2] = A[2] xor (A[0] or not A[1])
}

/*
 * theta does 
 *      A[0] = A[0] xor (A[0] rotl 16) xor (A[1] rotl 16) xor (A[2] rotl 16)
 *             xor (A[2] rotl 8) xor (A[0] shiftl 24) xor (A[0] shiftl 8)
 *             xor (A[1] shiftr 24) xor (A[2] shiftr 8)
 * and repeats with a circular shift on the indeces 
 */
 void theta( word * a){	
 	a[0] = a[0] ^ ((a[0] << 16) ^ (a[0] >> 16)) ^ ((a[1] << 16) ^ (a[1] >> 16))
 	            ^ ((a[2] << 16) ^ (a[2] >> 16)) ^ ((a[2] << 8 ) ^ (a[2] >> 24))
 	            ^ ( a[0] << 24) ^ (a[0] << 8 )  ^ ( a[1] >> 24) ^ (a[2] >> 8);

 	a[1] = a[1] ^ ((a[1] << 16) ^ (a[1] >> 16)) ^ ((a[2] << 16) ^ (a[2] >> 16))
 	            ^ ((a[0] << 16) ^ (a[0] >> 16)) ^ ((a[0] << 8 ) ^ (a[0] >> 24))
 	            ^ ( a[1] << 24) ^ (a[1] << 8 )  ^ ( a[2] >> 24) ^ (a[0] >> 8);

    a[2] = a[2] ^ ((a[2] << 16) ^ (a[2] >> 16)) ^ ((a[0] << 16) ^ (a[0] >> 16))
 	            ^ ((a[1] << 16) ^ (a[1] >> 16)) ^ ((a[1] << 8 ) ^ (a[1] >> 24))
 	            ^ ( a[2] << 24) ^ (a[2] << 8 )  ^ ( a[0] >> 24) ^ (a[1] >> 8);
 }

 /*
  * Generate the round keys 
  */
 void generateRoundKeys(word z, word * e){
 	for( int i = 0; i < 11; i++ ){
 		e[i] = z;
 		z = z << 1;
 		if(z & 0x10000){
 			z = z ^ 0x11011;
 		}
 	} 
 }

 /*
  * The encrypt function will encrypt the message
  */
 void encrypt( word * a, word * k){
 	word roundKey[12];

 	generateRoundKeys(st_enc, roundKey); 
 	for( int i = 0; i < 11; i++ ){
 		a[0] = a[0] ^ k[0] ^ (roundKey[i] << 16);
 		a[1] = a[1] ^ k[1];
 		a[2] = a[2] ^ k[2] ^ (roundKey[i]);
 		theta(a);
 		pi1(a);
 		gamma(a);
 		pi2(a);
 	}

 	a[0] = a[0] ^ k[0] ^ (roundKey[11] << 16);
 	a[1] = a[1] ^ k[1];
 	a[2] = a[2] ^ k[2] ^ (roundKey[11]);
 	theta(a);
 }