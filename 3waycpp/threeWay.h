/*
 * Author: Roger Barker
 * Due: 7/15/2014
 * CS4780 p1
 * 3 way encryption algorithm
 */

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <stdio.h>
using namespace std;

//definitions
#define st_enc 0x0b0b //constant for first round of encryption
typedef unsigned long int word ;

string convertHexToAscii(string hexVal); //converts a hex string to ascii

void gamma(word * a); 	//does a bunch of xors ors and nots
void theta(word * a); 	//does a bunch of xors and rotates and circular shifts
void pi1(word * a);		//does A0 = A0 rotr 10 and A2 = A2 rotl 1.
void pi2(word * a);		//does the reverse rotation: A0 is rotated 10 
						//to the left and A2 is rotated 1 to the right.

void generateRoundKeys(word z, word * e); //function to generate the round generateRoundKeys
void encrypt( word * a, word * k);