#Authored by Roger Barker

CC = g++
TARGET = 3way
OBJS = threeWay.o
HEAD = threeWay.h
.SUFFIXES: .cpp .o

$(TARGET) : $(OBJS)
	$(CC) -o $(TARGET) $(OBJS)

.c.o : 
	$(CC) -c $<

clean :
	/bin/rm -f *.o $(TARGET)

